<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:36
 */

namespace ch\tbz\rockpaperscissors\player;

use ch\tbz\rockpaperscissors\route\Paper;
use ch\tbz\rockpaperscissors\route\Rock;
use ch\tbz\rockpaperscissors\route\Scissor;

class Computer extends User
{
    /**
     * Computer constructor.
     */
    public function __construct()
    {
        parent::setAction($this->generateAction());
    }

    /**
     * @return Paper|Rock|Scissor
     * @throws \ch\tbz\rockpaperscissors\exception\NoActionSetException
     */
    public function generateAction() {
        $random = rand(0 , 2);
        return $this->getActionById($random);
    }
}