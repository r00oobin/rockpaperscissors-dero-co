<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:35
 */

namespace ch\tbz\rockpaperscissors\player;

use ch\tbz\rockpaperscissors\exception\NoActionSetException;
use ch\tbz\rockpaperscissors\route\Action;
use ch\tbz\rockpaperscissors\route\Paper;
use ch\tbz\rockpaperscissors\route\Rock;
use ch\tbz\rockpaperscissors\route\Scissor;

class User
{
    /**
     * @var Action
     */
    private $action;


    /**
     * @param $actionId
     * @return Paper|Rock|Scissor
     * @throws NoActionSetException
     */
    public function getActionById($actionId){
        switch ($actionId) {
            case 0:
                return new Rock();
            case 1:
                return new Paper();
            case 2:
                return new Scissor();
            default:
                throw new NoActionSetException("actionId Error");
        }
    }

    /**
     * @param $action Action
     * @return int ActionId
     * @throws NoActionSetException
     */
    public function getIdByAction($action){
        $class = get_class($action);
        switch ($class) {
            case Rock::class:
                return 0;
            case Paper::class:
                return 1;
            case Scissor::class:
                return 2;
            default:
                throw new NoActionSetException("actionId Error");
        }
    }

    /**
     * @return Action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param Action $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
}