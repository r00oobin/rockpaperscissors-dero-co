<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:37
 */

namespace ch\tbz\rockpaperscissors\route;


class Scissor extends Action
{
    function __construct()
    {
        $this->setLosingAgainst(Rock::ACTIONID);
        $this->setWinningAgainst(Paper::ACTIONID);
    }

    public function getActionId(){
        return Scissor::ACTIONID;
    }

    const ACTIONID = 2;
}