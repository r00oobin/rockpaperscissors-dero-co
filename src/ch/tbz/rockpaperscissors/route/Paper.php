<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:36
 */

namespace ch\tbz\rockpaperscissors\route;


class Paper extends Action
{
    function __construct()
    {
        $this->setLosingAgainst(Scissor::ACTIONID);
        $this->setWinningAgainst(Rock::ACTIONID);
    }

    public function getActionId(){
        return Paper::ACTIONID;
    }

    const ACTIONID = 1;
}