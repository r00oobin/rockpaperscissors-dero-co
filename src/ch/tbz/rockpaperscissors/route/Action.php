<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 15:01
 */

namespace ch\tbz\rockpaperscissors\route;


class Action
{
    /**
     * @var integer
     */
    private $winningAgainst;

    /**
     * @var integer
     */
    private $losingAgainst;

    /**
     * @return int
     */
    public function getWinningAgainst()
    {
        return $this->winningAgainst;
    }

    /**
     * @param int $winningAgainst
     */
    public function setWinningAgainst($winningAgainst)
    {
        $this->winningAgainst = $winningAgainst;
    }

    /**
     * @return int
     */
    public function getLosingAgainst()
    {
        return $this->losingAgainst;
    }

    /**
     * @param int $losingAgainst
     */
    public function setLosingAgainst($losingAgainst)
    {
        $this->losingAgainst = $losingAgainst;
    }
}