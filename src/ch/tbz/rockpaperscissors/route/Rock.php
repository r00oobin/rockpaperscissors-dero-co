<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:36
 */

namespace ch\tbz\rockpaperscissors\route;


class Rock extends Action
{
    function __construct()
    {
        $this->setLosingAgainst(Paper::ACTIONID);
        $this->setWinningAgainst(Scissor::ACTIONID);
    }

    public function getActionId(){
        return Rock::ACTIONID;
    }

    const ACTIONID = 0;
}