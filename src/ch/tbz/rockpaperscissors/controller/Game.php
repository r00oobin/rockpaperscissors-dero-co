<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:34
 */

namespace ch\tbz\rockpaperscissors\controller;

use ch\tbz\rockpaperscissors\player\User;

class Game
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var User
     */
    private $computer;

    /**
     * Game constructor.
     * @param $user User
     * @param $computer User
     */
    function __construct($user, $computer)
    {
        $this->user = $user;
        $this->computer = $computer;
    }

    /**
     * @return bool|null
     * true = player win, false = computer win, null = draw (like this noob logan paul and ksi)
     */
    public function getWinner(){
        if($this->user->getAction()->getLosingAgainst() == $this->computer->getAction()->getActionId()){
            $_SESSION['user'] = 0;
            if(!isset($_SESSION['computer'] )){
                $_SESSION['computer'] = 0;
            }
            $_SESSION['computer'] = $_SESSION['computer'] + 1;
            return false;
        } else if($this->user->getAction()->getWinningAgainst() == $this->computer->getAction()->getActionId()){
            $_SESSION['computer'] = 0;
            if(!isset($_SESSION['user'] )){
                $_SESSION['user'] = 0;
            }
            $_SESSION['user'] = $_SESSION['user'] + 1;
            return true;
        } else {
            return null;
        }
    }
}