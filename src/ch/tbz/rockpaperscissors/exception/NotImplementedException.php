<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 04.12.2018
 * Time: 13:51
 */

namespace ch\tbz\rockpaperscissors\exception;


class NotImplementedException extends \BadMethodCallException
{}