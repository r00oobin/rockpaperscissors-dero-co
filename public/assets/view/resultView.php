<html>
<head>
    <title>Rock Paper Scissors</title>
    <meta name="description" content="Rock Paper Scissors Game">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-theme.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/stylesheet.css">
    <script src="/assets/js/actionPress.js"></script>
    <script src="/assets/js/animation.js"></script>
    <script>
        pressEnable = false;
        userAction = <?php echo $data['userAction']; ?>;
        computerAction = <?php echo $data['computerAction']; ?>;
        newStreak = <?php echo "'" . $data['newStreak'] . "'"; ?>;
    </script>
</head>
<body onload="start()">
<div class="winner" id="winner">
    <?php echo $data['winner']; ?>
</div>
<div class="container">
    <img src="/assets/img/logo.png" alt="Logo Image" class="logo">
    <div class="scoreBoard">
        <?php echo $_SESSION['oldStreak']; $_SESSION['oldStreak'] = $data['newStreak']; ?>
    </div>
    <div class="row">
        <div class="col-lg-12 col-xs-12">
            <div class="title">
                Rock Paper Scissors
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-0"></div>
        <div class="col-xs-4 col-sm-4 col-md-0">
            <div class="action paper" id="1" onclick="actionPress(this)">
                <img src="/assets/img/1.png" alt="Paper Icon" class="actionImgPaper">
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-0"></div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-4 col-sm-6">
            <div class="action rock" id="0" onclick="actionPress(this)">
                <img src="/assets/img/0.png" alt="Rock Icon" class="actionImgRock">
            </div>
        </div>
        <div class="col-md-4 col-sm-0">
            <div class="action paper" id="1" onclick="actionPress(this)">
                <img src="/assets/img/1.png" alt="Paper Icon" class="actionImgPaper">
            </div>
        </div>
        <div class="col-xs-6 col-md-4 col-sm-6">
            <div class="action scissor" id="2" onclick="actionPress(this)">
                <img src="/assets/img/2.png" alt="Scissor Icon" class="actionImgScissor">
            </div>
        </div>
    </div>
</div>
</body>
</html>