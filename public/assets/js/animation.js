function drawComputerAction(){
    var img = document.createElement("IMG");
    img.id = "computerImg";
    img.src = "/assets/img/0.png";
    img.className = "computerAction";
    document.body.appendChild(img);

    $("#computerImg").fadeIn();

    var left = $('#computerImg').offset().left;

    $("#computerImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "70%"}, "fast");
    $("#computerImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "30%"}, "fast");
    $("#computerImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "70%"}, "fast");
    $("#computerImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "30%"}, "fast");
    $("#computerImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "70%"}, "fast");
    $("#computerImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "50%"}, "fast");

}

function drawUserAction(){
    var img = document.createElement("IMG");
    img.id = "userImg";
    img.src = "/assets/img/0.png";
    img.className = "userAction";
    document.body.appendChild(img);

    $("#userImg").fadeIn();

    var left = $('#userImg').offset().left;

    $("#userImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "30%"}, "fast");
    $("#userImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "70%"}, "fast");
    $("#userImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "30%"}, "fast");
    $("#userImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "70%"}, "fast");
    $("#userImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "30%"}, "fast");
    $("#userImg").css({left:left})  // Set the left to its calculated position
        .animate({"left": "50%"}, "fast");
}

function setComputer(actionId) {
    $("#computerImg").attr("src","/assets/img/" + actionId + ".png");
}
function setUser(actionId) {
    $("#userImg").attr("src","/assets/img/" + actionId + ".png");
}
function setWinner() {
    $("#winner").fadeIn();
}

function setScore() {
    $(".scoreBoard")[0].innerText = newStreak;
    console.log(newStreak);
}

function start() {
    document.getElementsByClassName('container')[0].style.opacity = "0.3";
    setTimeout(drawUserAction(), 0);
    setTimeout(drawComputerAction(), 0);
    setTimeout(setUser, 1600, userAction);
    setTimeout(setComputer, 1600, computerAction);
    setTimeout(setWinner, 1800);
    setTimeout(setScore, 2300);
    setTimeout(end, 4500);
}

function end() {
    $("#computerImg").fadeOut();
    $("#userImg").fadeOut();
    $("#winner").fadeOut();
    setTimeout(displayBody(), 900);
    pressEnable = true;

}

function displayBody() {
    document.getElementsByClassName('container')[0].style.opacity = "1"
}