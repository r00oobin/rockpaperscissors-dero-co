<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 18.12.2018
 * Time: 14:28
 */

use ch\tbz\rockpaperscissors\controller\Game;

include "../autoload.php";

class Action extends Main
{
    /**
     * @var Game
     */
    public $game;

    public function __construct($actionId)
    {
        parent::__construct();
        $this->user->setAction(
            $this->user->getActionById($actionId)
        );
        $this->game = new Game($this->user, $this->computer);
    }

    public function displayHtml()
    {
        $data = [];
        $data['userAction'] = $this->user->getIdByAction($this->user->getAction());
        $data['computerAction'] = $this->computer->getIdByAction($this->computer->getAction());
        $result = $this->game->getWinner();
        if($result === null) {
            $data['winner'] = "Draw";
        } else if($result == false) {
            $data['winner'] = "CP WON";
        } else if($result == true){
            $data['winner'] = "USER WON";
        }
        parent::displayHtml($data, "resultView.php");
    }
}

$action = new Action($_GET['actionId']);
$action->displayHtml();