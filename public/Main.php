<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 18.12.2018
 * Time: 14:27
 */


use ch\tbz\rockpaperscissors\player\Computer;
use ch\tbz\rockpaperscissors\player\User;

include "../autoload.php";

class Main
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var Computer
     */
    public $computer;

    public function __construct()
    {
        $this->user = new User();
        $this->computer = new Computer();
    }

    public function getUserStreak(){
        if(!isset($_SESSION['user'])){
            $_SESSION['user'] = 0;
        }
        return $_SESSION['user'];
    }

    public function getComputerStreak(){
        if(!isset($_SESSION['computer'])){
            $_SESSION['computer'] = 0;
        }
        return $_SESSION['computer'];
    }

    /**
     * @param $data array
     * @param string $view
     */
    public function displayHtml($data = [], $view = "indexView.php"){
        if($this->getComputerStreak() > 0 ){
            $data['newStreak'] = "CP STREAK: " . $this->getComputerStreak();
        } else {
            $data['newStreak'] = "USER STREAK: " . $this->getUserStreak();
        }

        if(!isset($_SESSION['oldStreak'])){
            $_SESSION['oldStreak'] = "USER STREAK: 0";
        }

        include "assets/view/" . $view;
    }
}