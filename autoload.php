<?php
/**
 * Created by PhpStorm.
 * User: robin
 * Date: 18.12.2018
 * Time: 15:54
 */

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

spl_autoload_register(function ($class_name) {
    if($class_name == "Main"){
        $class = __DIR__ . "/public/" . $class_name . '.php';
        $class = str_replace('\\', '/', $class);
        require_once $class;
    } else {
        $class = __DIR__ . "/src/" . $class_name . '.php';
        $class = str_replace('\\', '/', $class);
        require_once $class;
    }
});